export class Employee {

    private _salary: number = null;

    public get salary(): number {
        if (this._salary === null) {
            // simulate havy operation ex. DB call, deep nested array operations
            let i = 0;
            while (i < 5000000000) {
                i++;
            }
            this._salary = i;
        }
        return this._salary;
    }
}

let e = new Employee();

[ 1, 2, 3, 4 ].forEach(n => {

    let start = new Date();

    console.log(e.salary);

    let delta = <number>(<any>new Date() - <any>start);
    if (delta >= 1000) {
        console.log(`#${n}: ${Math.round(delta / 1000)} seconds`);
    } else {
        console.log(`#${n}: ${Math.round(delta)} miliseconds`);
    }

    console.log('');
});
