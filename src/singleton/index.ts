export class Singleton1 {

    private static _instance: Singleton1 = null;

    private constructor() {
    }

    public static get instance() {
        if (this._instance === null) {
            this._instance = new this()
        }
        return this._instance;
    }
    
    public foo(): void {
        console.log('Singleton1 foo');
    }
}

export class Singleton2 {

    private static _instance: Singleton2 = new Singleton2();

    private constructor() {
        if (Singleton2._instance) {
            throw new Error("Error!!");
        }
        Singleton2._instance = this;
    }

    public static getInstance(): Singleton2 {
        return Singleton2._instance;
    }

    public foo(): void {
        console.log('Singleton2 foo');
    }
}

let s1 = Singleton1.instance;
s1.foo();

let s2 = Singleton2.getInstance();
s2.foo();