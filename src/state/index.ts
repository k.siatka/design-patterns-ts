export interface State {
    order: Order;
    cancelOrder(): void;
    verifyPayment(): void;
    shipOrder(): void;
}

export class Order {

    private currentState: State;
    public cancelledOrderState: State;
    public paymentPendingState: State;
    public orderShipedState: State;
    public orderBeingPrepared: State;

    constructor() {
        this.cancelledOrderState = new CancelledOrderState(this);
        this.paymentPendingState = new PaymentPendingState(this);
        this.orderShipedState = new OrderShippedState(this);
        this.orderBeingPrepared = new OrderBeingPrepared(this);

        this.setState(this.paymentPendingState);
    }

    public setState(state: State): void {
        this.currentState = state;
    }

    public getCurrentState(): State {
        return this.currentState;
    }
}

export class CancelledOrderState implements State {

    constructor(public order: Order) {
    }

    public cancelOrder(): void {
        console.log('This order is already cancelled');
        this.order.setState(this.order.cancelledOrderState);
    }

    public verifyPayment(): void {
        console.log('The order is cancelled, you cannot pay anymore.');
    }

    public shipOrder(): void {
        console.log('The order is cancelled, you cannot ship it anymore.');
    }
}

export class PaymentPendingState implements State {

    constructor(public order: Order) {
    }

    public cancelOrder(): void {
        console.log('Cancelling your unpaid order...');
        this.order.setState(this.order.cancelledOrderState);
    }

    public verifyPayment(): void {
        console.log('Payment verified! Shipping soon.');
        this.order.setState(this.order.orderBeingPrepared);
    }

    public shipOrder(): void {
        console.log('Cannot ship order when payment is pending!');
    }
}

export class OrderBeingPrepared implements State {

    constructor(public order: Order) {
    }

    public cancelOrder(): void {
        console.log('Cancelling your order.. You will be refunded.');
        this.order.setState(this.order.cancelledOrderState);
    }

    public verifyPayment(): void {
        console.log('Payment is already verified.');
    }

    public shipOrder(): void {
        console.log('Shipping your order now..');
        this.order.setState(this.order.orderShipedState);
    }
}

export class OrderShippedState implements State {

    constructor(public order: Order) {
    }

    public cancelOrder(): void {
        console.log('You cannot cancel an order that has been shipped.');
    }

    public verifyPayment(): void {
        console.log('Payment is already verified');
    }

    public shipOrder(): void {
        console.log('Order is already shipped');
    }
}

let order = new Order();

order.getCurrentState().verifyPayment();
order.getCurrentState().shipOrder();
order.getCurrentState().cancelOrder();

console.log('Order state: ' + (<any> order.getCurrentState()).constructor.name);