export interface Building {
    getType(): string;
}

export class House implements Building {
    public getType(): string {
        return 'house';
    }
}

export class Warehouse implements Building {
    public getType(): string {
        return 'warehouse'
    }
}

export class BuildingFactory {
    private static instances: { [key: string]: Building } = {
        'house': new House(),
        'warehouse': new Warehouse()
    };

    public static getBuilding<T extends Building>(type: string): T {
        return <T>BuildingFactory.instances[type];
    }
}

let building1: House = BuildingFactory.getBuilding('house');
console.log(building1.getType());

let building2: House = BuildingFactory.getBuilding('warehouse');
console.log(building2.getType());