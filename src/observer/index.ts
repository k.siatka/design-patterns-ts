export class Subject {
    private _subscribers: Observer[] = [];

    public register(o: Observer): void {
        this._subscribers.push(o);
        console.log(`Observer ${o.name} has been subscribed`);
    }

    public unregister(o: Observer): void {
        this._subscribers = this._subscribers.filter(s => s !== o);
        console.log(`Observer ${o.name} has been unsubscribed`);
    }

    public notify(data: any): void {
        this._subscribers.forEach(s => s.update(data));
    }
}

export class SimpleBehaviourSubject extends Subject {

    constructor(private value: any) {
        super();
    }

    public register(o: Observer): void {
        super.register(o);
        o.update(this.value);
    }

    public notify(data: any): void {
        this.value = data;
        super.notify(data);
    }
}

export class Observer {
    constructor(public name: string) {
    }

    public update(data: any): void {
        console.log(`Observer ${this.name}: ${data}`);
    }
}

console.log('----------SUBJECT----------');

let subject: Subject = new Subject();

let observer1 = new Observer('One');
let observer2 = new Observer('Two');
let observer3 = new Observer('Three');

subject.register(observer1);
subject.register(observer2);
subject.register(observer3);

subject.notify(new Date());

subject.unregister(observer1);
subject.unregister(observer3);

subject.notify(new Date());

console.log('----------SIMPLE BEHAVIOUR SUBJECT----------');

let bsubject: SimpleBehaviourSubject = new SimpleBehaviourSubject('initial value');
bsubject.register(observer1);
bsubject.register(observer2);

bsubject.notify('new value');
bsubject.register(observer3);