export class Person {

    constructor(public firstName: string, public lastName: string) {
    }

    public get fullName() {
        return `${this.firstName} ${this.lastName}`;
    }

    public sayHello(): string {
        return `Hello, I'm ${this.fullName}.`;
    }
}

let p = new Person('John', 'Doe');
console.log(p.sayHello());

// Only getter, cannot set!
// p.fullName = 'Test';


console.log('');


export class Person2 {

    constructor(public firstName: string, public lastName: string) {
    }

    public get fullName() {
        return `${this.firstName} ${this.lastName}`;
    }

    public set fullName(value: string) {
        const creds = value.split(' ');
        this.firstName = creds[0] || '';
        this.lastName = creds[1] || '';
    }
}

let e0 = new Person2('Dummy', 'Name');
// now we've got setter
e0.fullName = 'Chris Pratt';
console.log(`first: ${e0.firstName}, last: ${e0.lastName}`);


console.log('');


export class Employee extends Person {

    private _salary: number;

    constructor(firstName: string, lastName: string) {
        super(firstName, lastName);
    }

    public get salary(): number {
        return this._salary;
    }

    public set salary(value: number) {
        if (value < 1300) {
            throw new Error('Placa minimalna.');
        }
        this._salary = value;
    }

    public sayHello(): string {
        return `${super.sayHello()} I get paid ${this.salary} $.`;
    }
}

try {
    let e1 = new Employee('Jane', 'Doe');
    e1.salary = 2000;
    console.log(e1.sayHello());

    let e2 = new Employee('Tom', 'Jeff');
    e1.salary = 1000;
    console.log(e2.sayHello());


} catch (e) {
    console.error(e);
}

