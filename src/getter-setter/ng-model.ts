// mock NG
const Input = () => (target: any, propertyKey: string, descriptor: PropertyDescriptor) => {};
export interface OnInit  {}
export interface OnChanges  {}
export interface OnDestroy  {}
export interface ControlValueAccessor  {}
export interface Validator  {}


export class DatepickerComponent implements OnInit, OnChanges, OnDestroy, ControlValueAccessor, Validator {

    private dateVal: Date;

    @Input() public get date(): Date {
        return this.dateVal;
    }

    public set date(val: Date) {
        this.dateVal = val;
        //this.onChange(val);
        //this.onTouched();
    }

    public get selectedDay(): number {
        if (!this.date)
            return null;

        return this.date.getDate();
    }

    public set selectedDay(val: number) {
        if (!val)
            return;

        let newDate = new Date(this.dateVal || new Date());
        newDate.setDate(val);
        if (this.isDateValid(newDate)) {
            this.date = newDate;
        }
    }

    public get selectedMonth(): number {
        if (!this.date)
            return null;

        return this.date.getMonth();
    }

    public set selectedMonth(val: number) {
        if (!val)
            return;

        let newDate = new Date(this.dateVal || new Date());
        newDate.setMonth(val);
        if (this.isDateValid(newDate)) {
            this.date = newDate;
        }
    }

    public get selectedYear(): number {
        if (!this.date)
            return null;

        return this.date.getFullYear();
    }

    public set selectedYear(val: number) {
        if (!val)
            return;

        let newDate = new Date(this.dateVal || new Date());
        newDate.setFullYear(val);
        if (this.isDateValid(newDate)) {
            this.date = newDate;
        }
    }

    private isDateValid(date: Date): boolean {
        //return (!this.minYear || date.getFullYear() >= this.minYear) && (!this.maxYear || date.getFullYear() <= this.maxYear);
        return true;
    }

    // .... rest of code removed for this example to be more readable ....
}
