import { IProduct } from './base';
import * as Products from './products';
import * as Options from './options';

let orders: IProduct[] = [
    new Options.BasicShipment(
        new Options.Outlet(
            new Products.NewBalanceZante())),

    new Options.FastShipment(
        new Options.MemberDiscount(
            new Options.AdditionalLaces(
                new Products.AdidasEnergyBoost()))),

    new Options.InternationalShipment(
        new Products.NikeZoomPegasus()),

    new Products.HokaOneOne(),

    new Options.PresentWrapping(
        new Options.FastShipment(
            new Products.AsicsGelNimbus())),

    new Options.AdditionalSponge(
        new Options.FastShipment(
            new Options.MemberDiscount(
                new Options.Outlet(
                    new Products.SauconyKinvara())))),

    new Products.PumaFass600(),

    new Options.MemberDiscount(
        new Products.PopesKubota())
];

orders.forEach(o => console.log(`${o.getName()}: ${o.getPrice()}`));